package microgram.models;

import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;

@Document
public class Comment {

    private String CommentText;
    private LocalDateTime CommentTime;

    public Comment(User user, String text) {
        this.CommentText = text;
        CommentTime = LocalDateTime.now();
    }

    public String getText() {
        return CommentText;
    }

    public void setText(String text) {
        this.CommentText = text;
    }

    public LocalDateTime getCommentTime() {
        return CommentTime;
    }

    public void setCommentTime(LocalDateTime CommentTime) {
        this.CommentTime = CommentTime;
    }

}
