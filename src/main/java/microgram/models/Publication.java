package microgram.models;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Document
public class Publication {

    private String img;
    private String description;
    private LocalDateTime timeOfPublication;

    public Publication(String img, String description) {
        this.img = img;
        this.description = description;
        timeOfPublication = LocalDateTime.now();
    }

    @DBRef
    private List<Comment> comments = new ArrayList<>();
    private List<Like> likes = new ArrayList<>();

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getTimeOfPublication() {
        return timeOfPublication;
    }

    public void setTimeOfPublication(LocalDateTime PublicationTime) {this.timeOfPublication = PublicationTime;}

    public List<Comment> getComments() {return comments;}

    public void setComments(List<Comment> comments) {this.comments = comments;}
}
