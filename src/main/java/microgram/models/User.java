package microgram.models;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DBRef;
import java.util.ArrayList;
import java.util.List;

@Document
public class User {

    private String login;
    private String email;
    private String password;
    private List<Publication> publications = new ArrayList<>();
    @DBRef
    private List<User> followers = new ArrayList<>();
    @DBRef
    private List<User> subscriptions = new ArrayList<>();

    public User(String login, String email, String password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }

    public String getLogin() {return login;}

    public void setLogin(String login) {this.login = login;}

    public String getEmail() {return email;}

    public void setEmail(String email) {this.email = email;}

    public String getPassword() {return password;}

    public void setPassword(String password) {this.password = password;}

    public List<Publication> getPublications() {return publications;}

    public void setPublications(List<Publication> publications) {this.publications = publications;}

    public List<User> getFollowers() {return followers;}

    public void setFollowers(List<User> followers) {this.followers = followers;}

    public List<User> getSubscriptions() {return subscriptions;}

    public void setSubscriptions(List<User> subscriptions) {this.subscriptions = subscriptions;}

}
