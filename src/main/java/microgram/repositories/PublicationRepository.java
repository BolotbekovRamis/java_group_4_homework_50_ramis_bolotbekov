package microgram.repositories;

import microgram.models.Publication;
import org.springframework.data.repository.CrudRepository;

public interface PublicationRepository extends CrudRepository<Publication,String> {
}